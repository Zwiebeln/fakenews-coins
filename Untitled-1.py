import re
from urllib.request import urlopen
import numpy as np
from bs4 import BeautifulSoup

blogAddress = 'http://www.washingtonpost.com/blogs/right-turn/'

def articleUrls(blogAddress):
    html = urlopen(blogAddress).read()
    soup = BeautifulSoup(html,"lxml")
    links = soup.findAll("a")
    t = set([ link["href"] for link in links if link.has_attr('href')])
    l = []
    for s in t:
        a = s.split('/')
        if len(a)==11 and a[4]=='right-turn' and a[-1]!='#comments':
            l.append(s)
    return l

l = articleUrls(blogAddress)

clean = lambda s: str(re.sub('[\W_]+', ' ', s))

def getText(articleUrl):
    html = urlopen(articleUrl).read()
    soup = BeautifulSoup(html,"lxml")
    article = soup.body.findAll('article')
    text = ' '.join([clean(s.text) for s in article[0].findAll('p')])
    return text

blogs = {}
for i in l:
    text = getText(i)
    blogs[i] = text

    
f = open('/home/lukas/Projects/COINS/blogs/blogs.txt', "r+")
for i in blogs:
    f.write(i + '\t' + blogs[i]+'\n')
f.close()