import pandas as pd
import re
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from robobrowser import RoboBrowser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

clean = lambda s: str(re.sub('[\W_]+', ' ', s))


def getText(articleUrl):
    try:
        browser = RoboBrowser(history=False)
        browser.open(articleUrl)
        html = bs(str(browser.parsed), "html.parser")
        article = html.body.findAll('article')
        text = ' '.join([clean(s.text) for s in article[0].findAll('p')])
    except Exception:
        text = ''
    return text



df = pd.read_excel("wahington_post_articles.xlsx",
                   skiprows=1, sheetname="Sheet1")
df.columns=['id', 'url']
urls = df['url'].values

texts = []

for i in range(len(urls)):
    print(urls[i])
    texts.append(getText(urls[i]))


textDf = pd.DataFrame(texts)
textDf.columns = ['Articles']
textDf.to_excel('wahington_post_articles3.xlsx')