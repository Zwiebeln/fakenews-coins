import re
import numpy as np
import pandas as pd
from robobrowser import RoboBrowser
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


articlesHref = []

def getHtml(url):
    driver = webdriver.Firefox()
    driver.get(url)
    print(url)
    try:
        myElem = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'pb-results-container')))
        print("Page is ready!")
    except TimeoutException:
        print("Loading took too much time!")
        driver.close()
    source = bs(driver.page_source, 'html.parser')
    driver.close()
    return source



def extractArticleUrls(url):
    html = getHtml(url)
    resultContainer = html.find("div", {"class": "pb-results-container"})
    links = resultContainer.find_all("a")
    t = set([link["href"] for link in links if link.has_attr('href')])
    a = []
    for s in t:
        a.append(s)
    return a

allUrls = []
for i in range(100):
    url = 'https://www.washingtonpost.com/newssearch/?datefilter=All%20Since%202005&query=amazon&sort=Date&startat=' +  str(i*20) + '&utm_term=.548cf531529a&contenttype=Article'
    allUrls = allUrls + extractArticleUrls(url)

allUrls = list(filter(None, allUrls))
df = pd.DataFrame(allUrls)
df.columns = ['Url']
df.to_excel('wahington_post_articles3.xlsx')






"""   
urls = list(filter(None, urls))
df = pd.DataFrame(urls)
df.columns = ['Url']
df.to_excel('wahington_post_articles.xlsx')
"""

"""
articlesHref = list(filter(None, articlesHref))
df = pd.DataFrame(articlesHref)
df.columns = ['Url']
df.to_excel('wahington_post_articles.xlsx')
"""

